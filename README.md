Maths
=====

This a repo for maths.

GitLab flavored Markdown
[supports](https://docs.gitlab.com/ee/user/markdown.html#math) 
[KaTeX](https://github.com/KaTeX/KaTeX), a subset of
[LaTeX](https://github.com/latex3/latex2e).

Examples:

This math is inline $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```

### KaTeX / LaTeX Resources ###

- [KaTeX Supported Features][ktex01]
- [2-page LaTeX 2 Cheat Sheet][gith01]
- [Q: What does a double ampersand (&&) mean in LaTeX (*tex.stackexchange*)][tsex01]
- [The very short guide to typesetting with LaTeX (*pdf*)][silm01]


[gith01]: http://wch.github.io/latexsheet/latexsheet.pdf
[ktex01]: https://katex.org/docs/supported.html#vertical-layout
[silm01]: http://latex.silmaril.ie/veryshortguide/veryshortguide.pdf
[tsex01]: https://tex.stackexchange.com/questions/159723/what-does-a-double-ampersand-mean-in-latex