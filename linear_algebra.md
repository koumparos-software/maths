Linear Algebra
==============

<a name="0"> </a>
0: Index
--------

- [0: Index](#0)
- [1: Vectors](#1)
- [2: Real Co-Ordinate Space](#2)
- [3: Vector Arithmetic](#3)
- ...
- [n: Linear Subspaces](#n)

<a name="1"> </a>
1: Vectors
----------

Vectors have a magnitude and direction.  

Example:

```math
\overrightarrow{v} = (5,0) = \begin{bmatrix}5\\0\end{bmatrix}
```

<a name="2"> </a>
2: Real Co-ordinate Space
--------------------------

Often denoted like this: $`\R^n`$ where $`n`$ refers to the number of
dimensions. In the case of the 2D Real Co-ordinate Space ($`\R^2`$), this refers
to all possible real-valued 2-tuples.


<a name="3"> </a>
3: Vector Arithmetic
--------------------

### Addition ###

```math
\begin{aligned}

\overrightarrow{a} = \begin{bmatrix}\phantom{-}6\\-2\end{bmatrix} \\
\overrightarrow{b} = \begin{bmatrix}-4\\-4\end{bmatrix} \\
\overrightarrow{a} + \overrightarrow{b} = \begin{bmatrix}\phantom{-}2\\-6\end{bmatrix}

\end{aligned}
```
(insert image of graph showing vectors on 2D co-ordinate space)

### Multiplication by Scalar ###

```math
\begin{aligned}

\overrightarrow{a} = \begin{bmatrix}\phantom{-}6\\-2\end{bmatrix} \\
3\overrightarrow{a} = \begin{bmatrix}18\\-6\end{bmatrix}

\end{aligned}
```

Note that the operation (multiplication by a positive value) changed the
magnitude but not the direction. Multiplication by a negative value reverses
the direction

<a name="n"> </a>
n: Linear Subspaces
-------------------

### Subspace of $`\Reals^n`$ ###

$`\Reals^n`$ can be understood as a set of at least $`n`$ vectors each with
$`n`$ components that spans $`\Reals^n`$, or at least $`n`$ vectors are
linearly independent.

Let $`V`$ be a subset of $`\Reals^n`$.

Then for $`V`$ to be a subspace of $`\Reals^n`$:

1. $`V`$ contains **0** ($`\begin{bmatrix}0 \\ 0 \\ ... \\ n \end{bmatrix}`$)
2. for any $`\overrightarrow{x}`$ in $`V`$ then $`\overrightarrow{cx}`$ in 
   $`V`$ (closure under scalar multiplication)
3. for $`\overrightarrow{a}`$ in $`V`$ and $`\overrightarrow{b}`$ in $`V`$ then
   $`\overrightarrow{a} + \overrightarrow{b}`$ in $`V`$ (closure under addition)


"*basis*": the minimum set of vectors that spans a subspace.
